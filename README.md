# Bravo Aceros Back y Web :3

Comandos basicos para trabajar con este proyecto.

## Comandos básicos
```bash

# Crear migracion(make:migration) y la tabla(--create)
$ php artisan make:migration create_"Nombre(plural)"_table --create="Nombre(singular)"

# Crear modelo(make:model) y migracion (-m)
$ php artisan make:model "Nombre(singular)" -m

# Crear controlador
$ php artisan make:controller "Nombre+Controller(Singular)"

# Migrar BD
$ php artisan migrate

# Cargar la base de datos
$ docker-compose up --build

# Subir los datos a GIT
$ git push origin develop

# Bajar los datos de GIT
$ git pull origin develop

# Cargar el servidor
$ php artisan serve

# Migrar y reemplazar seed
$ php artisan migrate:fresh --seed
```




Consultar documentación #encasode
[Documentacion Laravel 7.x](https://laravel.com/docs/7.x/)

[SQLlog](https://github.com/webyog/sqlyog-community/wiki/Downloads)
