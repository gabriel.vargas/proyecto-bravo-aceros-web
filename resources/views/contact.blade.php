@extends('layouts.html')

@section('content')
@include('layouts.header')
@include( 'layouts.title', [ 'title' => $seo['title'] ] )

<section id="contact" class="contact">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="heading heading-4">
          <div class="heading-bg heading-right">
            <p class="mb-0"></p>
            <h2>Contáctanos</h2>
          </div>
        </div>
        <!-- .heading end -->
      </div>
      <!-- .col-md-12 end -->
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-4 widgets-contact mb-60-xs">
            <div class="widget">
              <div class="widget-contact-icon pull-left">
                <i class="lnr lnr-map"></i>
              </div>
              <div class="widget-contact-info">
                <p class="text-capitalize">Dirección</p>
                <p class="text-capitalize font-heading">{{$configs['ADDRESS']}}</p>
              </div>
            </div>
            <!-- .widget end -->
            <div class="clearfix">
            </div>
            <div class="widget">
              <div class="widget-contact-icon pull-left">
                <i class="lnr lnr-envelope"></i>
              </div>
              <div class="widget-contact-info">
                <p class="text-capitalize ">Email</p>
                <p class="text-capitalize font-heading">{{$configs['EMAIL']}}</p>
              </div>
            </div>
            <!-- .widget end -->
            <div class="clearfix">
            </div>
            <div class="widget">
              <div class="widget-contact-icon pull-left">
                <i class="lnr lnr-phone"></i>
              </div>
              <div class="widget-contact-info">
                <p class="text-capitalize">Llámanos</p>
                <p class="text-capitalize font-heading">{{$configs['PHONE']}}</p>
              </div>
            </div>
            <!-- .widget end -->
          </div>
          <!-- .col-md-4 end -->
          <div class="col-xs-12 col-sm-12 col-md-8">
            <div class="row">
              @if (session('status'))
              <div class="alert">
                <div class="alert-icon">
                  <i class="fa fa-check-circle"></i>
                </div>
                <div class="alert-content">
                  <h4>{{ session('status') }}</h4>
                  <p>Nos pondremos en contacto con usted</p>
                </div>
              </div>
              @else
              <form id="contact-form" action="{{ route('send-contact') }}" method="post">
                @csrf
                <div class="col-md-6">
                  <input type="text" class="form-control mb-30" name="name" id="name" placeholder="Nombre" required />
                </div>
                <div class="col-md-6">
                  <input type="email" class="form-control mb-30" name="email" id="email" placeholder="Email" required />
                </div>
                <div class="col-md-6">
                  <input type="text" class="form-control mb-30" name="telephone" id="telephone" placeholder="Teléfono"
                    required />
                </div>
                <div class="col-md-6">
                  <input type="text" class="form-control mb-30" name="subject" id="subject" placeholder="Asunto" />
                </div>
                <div class="col-md-12">
                  <textarea class="form-control mb-30" name="message" id="message" rows="2" placeholder="Mensaje"
                    required></textarea>
                </div>
                <div class="col-md-12">
                  <button type="submit" class="btn btn-primary btn-black btn-block">Enviar
                    Correo</button>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 mt-xs">
                  <!--Alert Message-->
                  <div id="contact-result">
                  </div>
                </div>
              </form>
              @endif

            </div>
          </div>
          <!-- .col-md-8 end -->
        </div>
        <!-- .row end -->
      </div>
      <!-- .col-md-12 end -->
    </div>
    <!-- .row end -->
  </div>
  <!-- .container end -->
</section>

<section class="google-maps pb-0 pt-0">
  <div class="container-fluid">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 pr-0 pl-0">
        <div id="googleMap" style="width:100%;height:240px;">
        </div>
      </div>
    </div>
  </div>
</section>

@include('layouts.footer')
@endsection

@section('scripts')
<script type="text/javascript"
  src="http://maps.google.com/maps/api/js?sensor=true&key=AIzaSyDrb54820TWLlQkUK_VWS6dt3DKJL4sY7I"></script>
<script type="text/javascript" src="assets/js/jquery.gmap.min.js"></script>
<script type="text/javascript">
  $('#googleMap').gMap({
      address: "Uruguay 1753, San Ramón, Chile",
      zoom: 15,
      markers:[
          {
              address: "Uruguay 1753, San Ramón, Chile",
              maptype:'ROADMAP',
          }
      ]
  });
</script>
@endsection