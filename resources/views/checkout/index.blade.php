@extends('layouts.html')

@section('content')
@include('layouts.header')
@include('layouts.title', [ 'title' => $seo['title'] ] )
<style>
  .form-group input {
    margin-bottom: 10px;
  }

  .payment-methods .item {
    border-radius: 4px;
    border: 1px #ececec solid;
    padding: 24px;
    font-size: 14px;
    font-weight: 500;
    cursor: pointer;
    width: 100%;
  }
  
  .payment-methods label {
    width: 100%;
  }

  .payment-methods .item p {
    font-size: 12px;
    font-weight: 400;
    margin: 0;
  }
</style>
<section>
  <div class="container">
    <form action="{{ route('create-order') }}" enctype="multipart/form-data" method="POST">
      @csrf
      <div class="row justify-content-center">
        @if (!Auth::user())
        <div class="col-xs-12">
          <h3 class="text-uppercase">Mis datos</h3>
          <div class="row">
            <div class="col-xs-12 col-sm-6">
              <div class="form-group @error('name') has-error @enderror">
                <label for="name">Nombre y apellido</label>
                <input id="name" type="text" class="form-control" value="{{ old('name') }}" name="name" required>
                @error('name')
                <span class="help-block">{{ $message }}</span>
                @enderror
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group @error('rut') has-error @enderror">
                <label for="rut">Rut</label>
                <input id="rut" type="text" class="form-control" value="{{ old('rut') }}" name="rut" required>
                @error('rut')
                <span class="help-block">{{ $message }}</span>
                @enderror
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group @error('email') has-error @enderror">
                <label for="email">Email</label>
                <input id="email" type="email" class="form-control" value="{{ old('email') }}" name="email" required>
                @error('email')
                <span class="help-block">{{ $message }}</span>
                @enderror
              </div>
            </div>
            <div class="col-xs-12 col-sm-6">
              <div class="form-group @error('phone') has-error @enderror">
                <label for="telefono">Telefono</label>
                <input id="telefono" type="phone" class="form-control" value="{{ old('phone') }}" name="phone" required>
                @error('phone')
                <span class="help-block">{{ $message }}</span>
                @enderror
              </div>
            </div>
            <div class="col-xs-12">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" value="1" name="create-account" id="create-account"
                  @if(old('create-account')) checked @endif>
                <label class="form-check-label" for="create-account">
                  ¿Crear una cuenta?
                </label>
              </div>
            </div>
          </div>
        </div>
        @endif
        <div class="col-xs-12" style="margin-top: 50px;">
          <h3 class="text-uppercase">Facturacion</h3>
          <div class="row">
            <div class="col-xs-12 col-sm-6">
              <label for="payment_method_0">
                <div class="item">
                  <input type="radio" id="payment_method_0" name="facturacion" value="0" required @if(old('facturacion')==0) checked @endif>
                  Boleta
                </div>
              </label>
              <label for="factura_method_1">
                <div class="item" style="margin-left:10px;">
                  <input type="radio" id="factura_method_1" name="facturacion" value="1" required @if(old('facturacion')==1) checked @endif>
                  Factura
                </div>
              </label>
            </div>
            <div class="col-xs-12 col-sm-12" id="facturacion_zone" style="@if(old('facturacion')==1) display:block; @else display:none; @endif ">
              <div class="row" style="margin-top: 30px;">
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group @error('razon_social') has-error @enderror">
                    <label for="razon_social">Razon Social</label>
                    <input id="razon_social" type="text" class="form-control" value="{{ old('razon_social') }}" name="razon_social">
                    @error('razon_social')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                  </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                  <div class="form-group @error('facturacion_rut') has-error @enderror">
                    <label for="facturacion_rut">Rut Empresa</label>
                    <input id="facturacion_rut" type="text" class="form-control" value="{{ old('facturacion_rut') }}" name="facturacion_rut">
                    @error('facturacion_rut')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12">
                  <div class="form-group @error('giro') has-error @enderror">
                    <label for="giro">Giro</label>
                    <input id="giro" type="text" class="form-control" value="{{ old('giro') }}" name="giro">
                    @error('giro')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                  <div class="form-group @error('direccion') has-error @enderror">
                    <label for="direccion">Direccion </label>
                    <input id="direccion" type="text" class="form-control" value="{{ old('direccion') }}" name="direccion">
                    @error('direccion')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                  <div class="form-group @error('region') has-error @enderror">
                    <label for="region">Region</label>
                    <select name="region" id="region" class="form-control" style="margin-bottom: 10px;">
                      @forelse ($states as $state)
                      <option value="{{ $state['id'] }}" @if(old('region')==$state['id']) selected @endif>{{ $state['name'] }}</option>
                      @empty
                      @endforelse
                    </select>
                    @error('region')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                  </div>
                </div>
                <div class="col-xs-12 col-sm-4">
                  <div class="form-group @error('comuna') has-error @enderror">
                    <label for="comuna">Comuna</label>
                    <select name="comuna" id="comuna" class="form-control" style="margin-bottom: 10px;">
                      @forelse ($states[0]['zones'] as $comuna)
                        <option value="{{ $comuna['id'] }}" @if(old('comuna')==$comuna['name']) selected @endif>{{ $comuna['name'] }} {{ $comuna }}</option>
                      @empty
                      @endforelse
                    </select>
                    @error('comuna')
                    <span class="help-block">{{ $message }}</span>
                    @enderror
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-12" style="margin-top: 50px;">
          <h3 class="text-uppercase">Tu pedido</h3>
          <div class="cart-table table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr class="cart-product">
                  <th class="cart-product-item" style="text-align: left;padding-left: 20px;">Producto</th>
                  <th class="cart-product-price">Precio</th>
                  <th class="cart-product-total">Total</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($cart['products'] as $product )
                <tr class="cart-product">
                  <td class="cart-product-item">
                    <div class="cart-product-name">
                      <h6>{{ $product['name'] }} x {{ $product['quantity'] }}</h6>
                    </div>
                  </td>
                  <td class="cart-product-price">$ {{ number_format($product['price']) }}</td>
                  <td class="cart-product-total">$ {{ number_format($product['price'] * $product['quantity']) }}</td>
                </tr>
                @empty
                <div class="alert alert-success" style="margin-bottom: 0;">
                  <p> No hay productos en su carro </p>
                </div>
                @endforelse
                <tr class="cart-product">
                  <td class="cart-product-item" style="text-align: right;" colspan="2">
                    <strong>Valor de despacho </strong>
                  </td>
                  <td class="cart-product-shipping" style="text-align: center;">
                    Por definir
                  </td>
                </tr>
                <tr class="cart-product">
                  <td class="cart-product-item" style="text-align: right;" colspan="2">
                    <strong>Total</strong>
                  </td>
                  <td class="cart-product-total-amount" style="text-align: center;">$
                    {{  number_format($cart['total']) }}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="col-xs-12">
          <h3 class="text-uppercase">Formas de pago</h3>
          <div class="row payment-methods">
            @foreach ($payments as $payment)
            <div class="col-xs-12 col-sm-4">
              <label for="payment_method_{{ $payment['id'] }}">
                <div class="item">
                  <input type="radio" id="payment_method_{{ $payment['id'] }}" name="payment_method" value="{{ $payment['id'] }}"
                    @if(old('payment_method')==$payment['id']) checked @endif required>
                  {{ $payment['name'] }}
                  <p>{{ $payment['description'] }}</p>
                </div>
              </label>
            </div>
            @endforeach

          </div>
        </div>
        <div class="col-xs-12">
          <div style="margin-top:24px;" class="product-cta text-right text-center-xs">
            <div class="form-check">
              <label class="form-check-label" for="terminos">
                Acepto los <a target="_blank" title="Terminos y condiciones de uso"
                  href="{{ route('cms', ['id' => 'condiciones-de-uso']) }}">terminos y condiciones de uso</a> de
                BravoAceros
              </label>
              <input class="form-check-input" type="checkbox" value="1" name="terminos" id="terminos"
                @if(old('terminos')==1) checked @endif required>
            </div>
            <button type="submit" class="btn btn-secondary" style="padding: 0 20px;width: auto;">
              Comprar y finalizar
            </button>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>
@include('layouts.footer')
@endsection

@section('scripts')
<script>
  const free_shipping = '{{ $configs['MAX_BUY'] }}';
  const total = '{{ $cart['total'] }}';
  const states = @json($states);
  const id_zone = @json($states);
  const id_comuna = {{ (old('comuna')) ? old('comuna') : 0 }};
  console.log(id_comuna)
  function hide_shipping(val) {
    if (val == 2) {
        $('#shipping_home').hide()
        $('#shipping_store').show()
        return;
      }
    $('#shipping_home').show()
    $('#shipping_store').hide()
  }
  function setComunas(val) {
    let zones = null
    let htmlZone = '';
    states.forEach(e => {
      if (val == e.id) {
        zones = e.zones
      }
    });
    if (zones) {
      zones.forEach((e) => {
        htmlZone += `<option value="${e.id}" ${(id_comuna === e.id) ? 'selected' : ''}>${e.name}</option>`
      });
      $("select[name='comuna']").html(htmlZone)
    }
  }
  $(document).ready(function() {
    setComunas($("select[name='region']").val());
    $("input[name='shipping_type']").change(function(){
      const val = $(this).val() 
      hide_shipping(val)
    });
    $("select[name='region']").change(function(){
      const val = $(this).val();
      setComunas(val);
    });
    /*
    $('.cart-product-shipping').html(`Seleccione una comuna`);
    $('.cart-product-total-amount').html(`$ ${total}`);

    const price = $("select[name='shipping_zone']").find('option:selected').data('price');
    $('.cart-product-shipping').html(`$ ${price.toLocaleString("es-ES")}`);
    $('.cart-product-total-amount').html(`$ ${(parseInt(price) + parseInt(total)).toLocaleString("es-ES")}`);
    
    const val = $("input[name='shipping_type']").val();
    hide_shipping(val)


    $("select[name='shipping_zone']").change(function(){
      const price = $(this).find('option:selected').data('price');
      $('.cart-product-shipping').html(`$ ${price.toLocaleString("es-ES")}`);
      $('.cart-product-total-amount').html(`$ ${(parseInt(price) + parseInt(total)).toLocaleString("es-ES")}`);
    });
    */
    $("input[name='facturacion']").change(function(){
      const val = $(this).val() 
      if (val == 1) {
        $('#facturacion_zone').show()
        return;
      }
      $('#facturacion_zone').hide()
    });
  }) 
</script>
@endsection