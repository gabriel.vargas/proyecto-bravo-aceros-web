@extends('layouts.html')

@section('content')
@include('layouts.header')
@include('layouts.title', [ 'title' => $seo['title'] ] )

<section class="asd">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-xs-12">
        @if (session('verified'))
          Su correo fue verificado
        @endif
        @if (session('step'))
          Enviamos un email a su correo para validar el registro, por favor ingrese a su correo
        @endif
      </div>
    </div>
  </div>
</section>

@include('layouts.footer')
@endsection