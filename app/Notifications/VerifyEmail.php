<?php
namespace App\Notifications;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Lang;
use Illuminate\Auth\Notifications\VerifyEmail as VerifyEmailBase;

class VerifyEmail extends VerifyEmailBase
{
//    use Queueable;

    // change as you want
    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable);
        }
        return (new MailMessage)
            ->greeting('Hola')
            ->subject('Confirme su dirección de correo electrónico')
            ->line('Haga clic en el botón de abajo para verificar su dirección de correo electrónico.')
            ->action(
                'Confirme su dirección de correo electrónico',
                $this->verificationUrl($notifiable)
            )
            ->line('Si no creó una cuenta, no se requiere ninguna otra acción.');
    }
}