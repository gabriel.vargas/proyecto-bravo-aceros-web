<?php

namespace App\Imports;

use App\Product;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Validation\Rule;


class ProductImport implements ToModel, WithHeadingRow
{
    private $idMain;

    public function __construct(int $idMain) 
    {
        $this->idMain = $idMain;
    }
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
      $product = new Product();
      $product->product_main_id = $this->idMain;
      $product->sku = $row['sku'];
      $product->name = $row['nombre'];
      $product->active = 1;
      $product->price = $row['precio'];
      $product->save();
    }
}
