<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
  protected $fillable = ['name', 'status'];

  public function zones()
  {
    return $this->hasMany(Zone::class, 'id_state');
  }
}
