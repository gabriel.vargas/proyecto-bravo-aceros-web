<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Provider;
use App\Stock;
use App\StockMovement;

class ProviderController extends Controller
{
  public function index(Request $request)
  {
    $all = Provider::paginate(30);
    if (isset($request->rut)) {
      $all = Provider::where('rut', 'like', "%{$request->rut}%")->get();
    }
    if (isset($request->actives)) {
      $all = Provider::where('active', $request->actives)->get();
    }
    return response()->json($all, 200);
  }

  public function store(Request $request)
  {
    $input = $request->only(['name', 'rut', 'email', 'active']);
    $item = Provider::create($input);
    return response()->json($item, 200);
  }

  public function show($id)
  {
    $item = Provider::find($id);
    return response()->json($item, 200);
  }

  public function update(Request $request, $id)
  {
    $input = $request->only(['name', 'rut', 'email', 'active']);
    $update = Provider::where('id', $id)->update($input);
    return response()->json($update, 200);
  }

  public function destroy($id)
  {
    $item = Provider::find($id);
    $item->delete();
    return response()->json($item, 200);
  }

  public function getTotal(Request $req) 
  {
    $stocks = StockMovement::query()
      ->with(['stock'])
      ->where('id_provider', $req->idProvider)
      ->whereBetween('created_at', [$req->from." 00:00:00", $req->end." 23:59:59"])
      ->orderBy('created_at', 'DESC')
      ->paginate(30);
    $count = StockMovement::where('id_provider', $req->idProvider)
      ->whereBetween('created_at', [$req->from." 00:00:00", $req->end." 23:59:59"])
      ->count();
    
    return response()->json([
      'stocks' => $stocks,
      'movement' => $count,
    ], 200);
  }
}
