<?php
namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\Comentario;
use App\Order;
use App\OrderHistory;
use App\StockMovement;
use App\Stock;

class OrderController extends Controller
{
  public function addHistory($id, $status, $msj, $notify) {
    $orderHistory = new OrderHistory();
    $orderHistory->id_order_status = $status;
    $orderHistory->id_order = $id;
    $orderHistory->message = $msj;
    $orderHistory->notify = $notify;
    $orderHistory->save();
  }

  public function index(Request $req)
  {
    $all = Order::paginate(30);
    if ($req->status && $req->fecha) {
      $date = Carbon::parse($req->fecha, 'UTC');
      $all = Order::with(['zone'])
        ->where('id_order_status', $req->status)
        ->whereDate('created_at', $date->isoFormat('YYYY-MM-DD'))
        ->paginate(50);
      if (!empty($req->search)) {
        $all = Order::with(['zone'])
          ->where('id_order_status', $req->status)
          ->whereDate('created_at', $date->isoFormat('YYYY-MM-DD'))
          ->where('name', 'like', "%$req->search%")
          ->orWhere('id', 'like', "%$req->search%")
          ->orWhere('total_pay', 'like', "%$req->search%")
          ->paginate(50);
      }
    }
    return response()->json($all, 200);
  }

  public function store(Request $request)
  {
    $input = $request->only([
      'method',
      'shipping_name',
      'shipping_address',
      'info_optional',
      'name',
      'email',
      'billing',
      'phone',
      'total_pay',
      'total_shipping',
      'total_discount',
      'code_uniq',
      'pay',
      'latitude',
      'longitude'
    ]);
    $item = Order::create($input);
    return response()->json($item, 200);
  }

  public function show($id)
  {
    $item = Order::with(['user', 'zone', 'payment', 'status', 'history', 'products'])->find($id);
    return response()->json($item, 200);
  }

  public function update(Request $request, $id)
  {
    $input = $request->only([
      'method',
      'shipping_name',
      'shipping_address',
      'info_optional',
      'name',
      'email',
      'billing',
      'phone',
      'total_pay',
      'total_shipping',
      'total_discount',
      'code_uniq',
      'pay',
      'latitude',
      'longitude'
    ]);
    $update = Order::where('id', $id)->update($input);
    return response()->json($update, 200);
  }

  public function destroy($id)
  {
    $item = Order::find($id);
    $item->delete();
    return response()->json($item, 200);
  }

  public function getOrderNumber(Request $req)
  {
    $date = Carbon::parse($req->fecha, 'UTC');
    $dateFormat = $date->isoFormat('YYYY-MM-DD');
    $pendiente = Order::where('id_order_status', 1)
      ->whereDate('created_at', $dateFormat)
      ->get();
    $pagados = Order::where('id_order_status', 2)
      ->whereDate('created_at', $dateFormat)
      ->get();
    $despacho = Order::where('id_order_status', 3)
      ->whereDate('created_at', $dateFormat)
      ->get();
    $entregados = Order::where('id_order_status', 4)
      ->whereDate('created_at', $dateFormat)
      ->get();
    return response()->json([
      'pendiente' => count($pendiente),
      'pagados' => count($pagados),
      'despacho' => count($despacho),
      'entregados' => count($entregados),
    ], 200);
  }

  public function updateUser(Request $req, $id) 
  {
    $input = $req->only([
      'email',
      'name',
      'phone'
    ]);
    $update = Order::where('id', $id)->update($input);
    return response()->json($update, 200);
  }

  public function updateAddress(Request $req, $id) 
  {
    $input = $req->only([
      'shipping_name',
      'shipping_address',
      'shipping_info',
      'id_zone',
      'total_shipping'
    ]);
    $update = Order::where('id', $id)->update($input);
    $order = Order::find($id);
    Order::where('id', $id)->update(['total_pay' => $order->total_shipping + $order->total_pay]);
    $this->addHistory($id, $order->id_order_status, 'Direccion actualizada', 0);
    return response()->json($update, 200);
  }

  public function updatePayment(Request $req, $id) 
  {
    $input = $req->only([
      'id_payment'
    ]);
    $update = Order::where('id', $id)->update($input);
    return response()->json($update, 200);
  }

  public function updateShipping(Request $req, $id)
  {
    $input = $req->only([
      'total_shipping'
    ]);
    Order::where('id', $id)->update($input);
    $order = Order::find($id);
    Order::where('id', $id)->update(['total_pay' => $order->total_shipping + $order->total_pay]);
    $this->addHistory($id, $order->id_order_status, "Valor de despacho actualizado a {$order->total_shipping}", 0);
    return response()->json($order, 200);
  }

  public function changeStatus(Request $req, $id)
  {
    $orderStatus = $req->status;
    $msj = $req->comentario;
    $order = Order::with(['products'])->find($id);
    if ($msj !== '') {
      $this->addHistory($id, $orderStatus, $msj, $req->notificar);
      Order::where('id', $id)->update(['id_order_status' => $orderStatus]);
      
      if($req->notificar) {
        Mail::to($order->email)->send(new Comentario([
          'name' => $order->name,
          'email' => $order->email,
          'id' => $order->id,
          'comentario' => $msj,
          'asunto' => "Información importante sobre su pedido {$order->id} - BravoAceros"
        ]));
        // Notificar Mensaje
      }

      if($req->stock) {
        $user = Auth::guard('api')->user();
        if ($user) {
          $id_store = $user->id_store;
          foreach ($order->products as $product) {
            $id_product = $product->id_product;
            $quantity = $product->quantity * -1;
            $stock = Stock::where('id_store', $id_store)->where('id_product', $id_product)->first();
            if (!$stock) {
              $stock = new Stock;
              $stock->id_store = $id_store;
              $stock->id_product = $id_product;
              $stock->saldo = 0;
              $stock->save();
            }
            $saldo = $stock->saldo + $quantity;
            $stockM = new StockMovement;
            $stockM->id_stock = $stock['id'];
            $stockM->description = "Descuento stock Pedido {$id}";
            $stockM->id_order = $id;
            $stockM->saldo = $saldo;
            $stockM->value = $quantity;
            $stockM->save();
            $stock->saldo = $saldo;
            $stock->save();
          }
        }
      }
    }
    return response()->json($order, 200);
  }
  
}
