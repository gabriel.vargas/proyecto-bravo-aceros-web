<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\State;

class StateController extends Controller
{
  public function index (Request $req) {
    $all = State::paginate(30);
    if ($req->all) {
      $all = State::where('status', 1)->get();
    }
    return response()->json($all, 200);
  }

  public function store (Request $request) {
    $input = $request->only(['name', 'status']);
    $seo = State::create($input);
    return response()->json($seo, 200);
  }

  public function show ($id) {
    $seo = State::find($id);
    return response()->json($seo, 200);
  }

public function update (Request $request, $id) {
    $input = $request->only(['name', 'status']);
    $update = State::where('id', $id)->update($input);
    return response()->json($update, 200);
}

public function destroy ($id) {
    $seo = State::find($id);
    $seo->delete();
    return response()->json($seo, 200);
}
}
