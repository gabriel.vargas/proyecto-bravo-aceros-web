<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use App\Seo;
use App\Category;
use App\Config;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
  
    public function configs()
    {
        $configs = Config::all();
        $c = [];
        foreach ($configs as $config) {
            $c[$config->name] = $config->value;
        }
        return $c;
    }

    public function getCart()
    {
        $cart = (session('cart')) ? session('cart') : [];
        $total = 0;
        foreach ($cart as $product) {
            $total += intval($product['quantity']) * intval($product['price']);
        }
        return [
            'products' => $cart,
            'total' => $total
        ];
    }

    public function showResetForm(Request $req)
    {
        $seo = Seo::where('url', '/olvide-mi-contrasena/reset')->first();
        $categories = Category::where('active', true)->get();
        return view('auth.passwords.reset', [
            'seo' => $seo,
            'token' => $req->token,
            'email' => $req->email,
            'categories' => $categories,
            'configs' => $this->configs(),
            'cart' => $this->getCart()
        ]);
    }

}
