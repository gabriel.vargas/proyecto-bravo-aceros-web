<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Seo;
use App\Category;
use App\Config;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\Register;
use Freshwork\ChileanBundle\Rut;

class RegisterController extends Controller
{
  /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

  use RegistersUsers;

  /**
   * Where to redirect users after registration.
   *
   * @var string
   */
  protected $redirectTo = '/';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('guest');
  }

  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array  $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
    return Validator::make(
      $data,
      [
        'name' => ['required', 'string', 'max:255'],
        'rut' => 'required|cl_rut',
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'password' => ['required', 'string', 'min:8', 'max:25', 'confirmed', 
          'regex:/[a-z]/',      // must contain at least one lowercase letter
          'regex:/[A-Z]/',      // must contain at least one uppercase letter
          'regex:/[0-9]/',      // must contain at least one digit
          'regex:/[@$!%*#?&]/' // must contain a special character
        ] 
      ],
      [
        'name.required' => 'El nombre es obligatorio',
        'name.min' => 'Minimo 5 caracteres',
        'name.max' => 'Maximo 255 caracteres',
        'rut.cl_rut' => 'Rut no valido',
        'rut.required' => 'Rut es requerido',
        'email.required' => 'El email es obligatorio',
        'email.email' => 'Debe tener formato email',
        'email.string' => 'Debe tener formato text',
        'email.max' => 'Maximo 255 caracteres',
        'email.unique' => 'Ya se encuentra registrado',
        'password.required' => 'Contraseña es requerido',
        'password.string' => 'Contraseña debe ser un texto',
        'password.min' => 'Minimo 8 caracteres',
        'password.confirmed' => 'Las contraseñas no son iguales',
        'password.regex' => 'Su contraseña debe tener un caracter especial, al menos una mayuscula y caracter númerico',
      ]
    );
  }

  /**
   * Create a new user instance after a valid registration.
   *
   * @param  array  $data
   * @return \App\User
   */
  public function register(Request $request)
  {
      $this->validator($request->all())->validate();
      event(new Registered($user = $this->create($request->all())));
      // $this->guard()->login($user);
      return $this->registered($request, $user) ?: redirect('gracias-por-registrarse')->with('step', 1);
   }

  protected function create(array $data)
  {
    Mail::to($data['email'])->send(new Register([
      'name' =>  $data['name']
    ]));
    return User::create([
      'name' => $data['name'],
      'email' => $data['email'],
      'rut' => Rut::parse(Rut::parse($data['rut'])->format())->number(),
      'phone' => $data['phone'],
      'password' => Hash::make($data['password']),
    ]);
  }

  public function getCart()
  {
    $cart = (session('cart')) ? session('cart') : [];
    $total = 0;
    foreach ($cart as $product) {
      $total += intval($product['quantity']) * intval($product['price']);
    }
    return [
      'products' => $cart,
      'total' => $total
    ];
  }

  public function configs()
  {
    $configs = Config::all();
    $c = [];
    foreach ($configs as $config) {
      $c[$config->name] = $config->value;
    }

    return $c;
  }

  public function showRegistrationForm()
  {
    $seo = Seo::where('url', '/registro')->first();
    $categories = Category::where('active', true)->get();
    return view('auth.register', [
      'seo' => $seo,
      'categories' => $categories,
      'configs' => $this->configs(),
      'cart' => $this->getCart()
    ]);
  }

  public function showVerifyForm()
  {
    $seo = Seo::where('url', '/registro')->first();
    $categories = Category::where('active', true)->get();
    return view('auth.verify', [
      'seo' => $seo,
      'categories' => $categories,
      'configs' => $this->configs(),
      'cart' => $this->getCart()
    ]);
  }
  
}
