<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Verified;
use App\User;
use App\Seo;
use App\Category;
use App\Config;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function getCart()
    {
      $cart = (session('cart')) ? session('cart') : [];
      $total = 0;
      foreach ($cart as $product) {
        $total += intval($product['quantity']) * intval($product['price']);
      }
      return [
        'products' => $cart,
        'total' => $total
      ];
    }
  
    public function configs()
    {
      $configs = Config::all();
      $c = [];
      foreach ($configs as $config) {
        $c[$config->name] = $config->value;
      }
  
      return $c;
    }
  
    public function show()
    {
      $seo = Seo::where('url', '/registro')->first();
      $categories = Category::where('active', true)->get();
      return view('auth.verify', [
        'seo' => $seo,
        'categories' => $categories,
        'configs' => $this->configs(),
        'cart' => $this->getCart()
      ]);
    }
    
    public function verify(Request $request)
    {   
        $user = User::find($request->route('id'));

        if ($user->hasVerifiedEmail()) {
            return redirect($this->redirectPath());
        }

        if ($user->markEmailAsVerified()) {
            event(new Verified($request->user()));
            return redirect('gracias-por-registrarse')->with('verified', true);
        }
    }
    
}
